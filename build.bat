echo on

pushd "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build"
call vcvarsall.bat %ARCH% -vcvars_ver=%VCVARS_VER%
popd

md src
curl -L https://boostorg.jfrog.io/artifactory/main/release/%BOOST_VER%/source/boost_%BOOST_VER:.=_%.tar.bz2 -o boost.tar.bz2 && ^
7z x boost.tar.bz2 -so | tar xf - -C src --strip-components=1 || goto :error

if %ARCH% == x86 (
    set AM=32
) else (
    set AM=64
)
set PYTHONPATH=C:/Python39_%ARCH%
set PATH=%PYTHONPATH:/=\%;%PATH%

pushd src
:: must be run on a separate process
cmd /c "bootstrap vc141" && ^
b2 --with-python --prefix="..\dist" address-model=%AM% variant=debug link=static threading=multi runtime-link=static install && ^
b2 --with-python --prefix="..\dist" address-model=%AM% variant=debug link=static threading=multi runtime-link=shared install && ^
b2 --with-python --prefix="..\dist" address-model=%AM% variant=release link=static threading=multi runtime-link=static install && ^
b2 --with-python --prefix="..\dist" address-model=%AM% variant=release link=static threading=multi runtime-link=shared install || goto :error
popd

md deploy
cd dist
rd /s/q lib\cmake
7z a ..\deploy\%DIST_NAME% .

goto :EOF

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
